//
//  LoginController.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
       
    
    let loginView = LoginView(frame: .zero)
    
    override func loadView() {
        self.view = loginView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView.loginFormView.delegate = self
    }
    
    func prepareLoginViewController() -> UINavigationController {
        let feedController = FeedController()
        let navigationController = UINavigationController(rootViewController: feedController)
        navigationController.modalPresentationStyle = .fullScreen
        return navigationController
    }
}

extension LoginController: LoginFormViewDelegate {
    func shouldLogin(email: String, password: String) {
        showLoadingView(superView: view)
        WebService.sharedInstance.login(credential: Credential(email: email, password: password)) { (result) in
            self.removeLoadingView()
            switch result {
            case .success(_):
                self.present(self.prepareLoginViewController(), animated: true, completion: nil)
            case .failure(_):
                self.loginView.loginFormView.shouldDisplayError()
            }
        }
    }
}


