//
//  EnterpriseDetailController.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseDetailController: UIViewController, ConfigurableView {

    let enterprise: Enterprise
    
    let enterpriseDetailView = EnterpriseDetailView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavigationBar()
        enterpriseDetailView.setupInformationOf(Enteprise: enterprise)
    }
    
    init(enterprise: Enterprise) {
        self.enterprise = enterprise
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupNavigationBar() {
        navigationItem.title = enterprise.enterpriseName
        if let navigationBar = navigationController?.navigationBar {
            navigationBar.layer.zPosition = 0
            
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = .white
            
            navigationBar.standardAppearance = appearance
            navigationBar.compactAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.layer.zPosition = -1
    }
 
    func buildViewHierarchy() {
        view.addSubview(enterpriseDetailView)
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            enterpriseDetailView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            enterpriseDetailView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            enterpriseDetailView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            enterpriseDetailView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        view.backgroundColor = .white
    }
}
