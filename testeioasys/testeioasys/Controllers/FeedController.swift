//
//  FeedController.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import UIKit

class FeedController: UIViewController, ConfigurableView {
    
    var isSearching: Bool = false

    let feedView = FeedView()
    
    var dataSource: EnterpriseDataSource? {
        didSet {
            if let dataSource = dataSource {
                feedView.enterpriseView.setupDataSource(dataSource: dataSource)
                feedView.enterpriseView.verifyIfShoudDisplayNoFoundLabel(isSearching: self.isSearching, dataSourceIsEmpty: dataSource.enterprises.isEmpty)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        feedView.delegate = self
        feedView.enterpriseView.delegate = self
        navigationController?.navigationBar.prefersLargeTitles = true
        setupNavigatonBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigatonBar()
    }
    
    func buildViewHierarchy() {
        view.addSubview(feedView)
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            feedView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            feedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            feedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            feedView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        
    }

}

extension FeedController: FeedViewDelegate {
    func didAdaptNavigation(state: NavigationState) {
        switch state {
        case .large:
            UIView.animate(withDuration: 0.5) {
                self.navigationController?.navigationBar.prefersLargeTitles = true
                self.isSearching = false
                self.view.layoutIfNeeded()
            }
        case .normal:
            UIView.animate(withDuration: 0.5) {
                self.navigationController?.navigationBar.prefersLargeTitles = false
                self.isSearching = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func didRecieveStringToSearch(value: String) {
        showLoadingView(superView: feedView, withBlur: false)
        WebService.sharedInstance.getCompanys(searchString: value) { (result) in
            switch result {
            case .success(let enterprises):
                self.dataSource = EnterpriseDataSource(enterprises: enterprises.enterprises)
            case .failure(_):
                return
            }
            self.removeLoadingView()
        }
    }
}

extension FeedController {
    func setupNavigatonBar() {
        navigationController?.view.backgroundColor = .white
        if let navigationBar = navigationController?.navigationBar {
            let bgImage = view.imageWithGradient(startColor: .initialGrandientColor, endColor: .finalGrandientColor, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor(patternImage: bgImage!)
            navigationBar.layer.zPosition = -1
            navigationBar.standardAppearance = appearance
            navigationBar.compactAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
        }
    }
}

extension FeedController: EnteprisiesViewDelegate {
    func selectedItem(_ item: Int) {
        if let enterprises = dataSource?.enterprises, let navController = navigationController {
            let enterprise = enterprises[item]
            let enterpriseDetailController = EnterpriseDetailController(enterprise: enterprise)
            navController.pushViewController(enterpriseDetailController, animated: true)
        }
    }
}
