//
//  ConfigurableView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

protocol ConfigurableView {
    func setupView()
    func buildViewHierarchy()
    func setupConstraint()
    func setupAdditionalConfiguration()
}

extension ConfigurableView {
    func setupView() {
        buildViewHierarchy()
        setupConstraint()
        setupAdditionalConfiguration()
    }
}
