//
//  APIRouter.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
    case post
    case put
    case delete
}

enum APIRouter {
    
    case login(credential: Credential)
    case getCompanies(byName: String, headers: [String: String])
    case getCompany(byIndex: Int, headers: [String: String])
    
    private var baseURL: String {
        return "https://empresas.ioasys.com.br/api"
    }
    
    private var apiVersion: String {
        return "v1"
    }
    
    private var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .getCompanies:
            return .get
        case .getCompany:
            return .post
        }
    }
    
    private var path: String {
        switch self {
        case .login:
            return "users/auth/sign_in"
        case .getCompanies(let byName, _):
            return "enterprises?enterprise_types=1&name=\(byName)"
        case .getCompany(let byIndex, _):
            return "enterprises/\(byIndex)"
        }
    }
    
    func request () throws -> URLRequest {
        
        guard let url = URL(string: "\(baseURL)/\(apiVersion)/\(path)") else {
            throw NetworkError.urlError
        }
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 10)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .login(let login):
            request.httpBody = try JSONEncoder().encode(login)
            return request
        case .getCompanies(_, let headers), .getCompany(_, let headers):
            request.addValues(values: headers)
            return request
        }
    }
    
}
