//
//  WebService.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

struct Resource<T: Codable> {
    let url: URL
}

enum NetworkError: Error {
    case urlError
    case decodeError
    case encodeError
    case taskCanceled
}

struct CustomHeader {
    
    let accessToken: String
    let client: String
    let uid: String
    
    init(response: HTTPURLResponse) {
        self.accessToken = response.value(forHTTPHeaderField: "access-token")!
        self.client = response.value(forHTTPHeaderField: "client")!
        self.uid = response.value(forHTTPHeaderField: "uid")!
    }
    
    func getHeaders() -> [String: String] {
        return [
            "access-token": accessToken,
            "client": client,
            "uid": uid
        ]
    }
    
}

class WebService {
    
    private init () { }

    static var sharedInstance = WebService()
    
    var task: URLSessionTask?
    
    var customHeader: CustomHeader?
    
    
    func request<T: Decodable>(router: APIRouter, completion: @escaping (Result<(T,HTTPURLResponse), NetworkError>) -> Void ) {
        do {
            task = try URLSession.shared.dataTask(with: router.request(), completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                        if (error as NSError?)?.code == NSURLErrorCancelled {
                            completion(.failure(.taskCanceled))
                        } else {
                            completion(.failure(.urlError))
                        }
                        return
                    }
                    do {
                        let result = try JSONDecoder().decode(T.self, from: data)
                        completion(.success((result, response)))
                    } catch {
                        completion(.failure(.decodeError))
                    }
                }
                })
            task?.resume()
        } catch {
            completion(.failure(.urlError))
        }
    }
    
    func login(credential: Credential, completion:@escaping (Result<LoginResponse, NetworkError>)->Void) {
        request(router: .login(credential: credential)) { (result: Result<(LoginResponse,HTTPURLResponse),NetworkError>) in
            switch result {
            case .success((let loginResponse, let httpResponse)):
                self.customHeader = CustomHeader(response: httpResponse)
                completion(.success(loginResponse))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
        
    func getCompanys(searchString: String, completion: @escaping (Result<EnterpriseResponse,NetworkError>) -> Void) {
        if let headers = customHeader?.getHeaders(), searchString != "" {
            request(router: .getCompanies(byName: searchString, headers: headers)) { (result: Result<(EnterpriseResponse, HTTPURLResponse), NetworkError>) in
                switch result {
                case .success((let enterprises, _)):
                    completion(.success(enterprises))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } else {
            completion(.success(EnterpriseResponse(enterprises: [])))
        }
    }
}
