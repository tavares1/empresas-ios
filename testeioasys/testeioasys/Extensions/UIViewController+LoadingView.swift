//
//  UIViewController+LoadingView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController {
    func showLoadingView(superView: UIView, withBlur: Bool = true, style: UIBlurEffect.Style = .dark) {
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60), type: .circleStrokeSpin, color: .activityIndicatorColor, padding: nil)
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView()
        blurEffectView.effect = withBlur ? blurEffect : nil
        blurEffectView.frame = superView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        activityIndicatorView.center = blurEffectView.contentView.center
        blurEffectView.contentView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        view.addSubview(blurEffectView)
    }
    
    func removeLoadingView() {
        view.subviews.forEach { (view) in
            if let blurEffectView = view as? UIVisualEffectView {
                blurEffectView.removeFromSuperview()
            }
        }
    }
}
