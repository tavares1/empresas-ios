//
//  URLRequest+AddValues.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

extension URLRequest {
    mutating func addValues(values: [String:String]) {
        values.forEach { (key,value) in
            self.addValue(value, forHTTPHeaderField: key)
        }
    }
}
