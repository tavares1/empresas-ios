//
//  UIView+AddSubviews.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

 
extension UIView {
    func addSubviews(_ views: [UIView]) {
        views.forEach { (view) in
            self.addSubview(view)
        }
    }
}
