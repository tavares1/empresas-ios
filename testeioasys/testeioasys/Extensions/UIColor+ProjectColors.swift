//
//  UIColor+ProjectColors.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let loginButtonColor = UIColor(red: 224, green: 30, blue: 105)
    static let formTextFieldBackground = UIColor(red: 245, green: 245, blue: 245)
    static let activityIndicatorColor = UIColor(red: 251, green: 219, blue: 231)
    static let initialGrandientColor = UIColor(red: 162, green: 55, blue: 126)
    static let finalGrandientColor = UIColor(red: 120, green: 51, blue: 179)
    static let colorsCell:[UIColor] = [
        UIColor(red: 121, green: 187, blue: 202),
        UIColor(red: 235, green: 151, blue: 151),
        UIColor(red: 144, green: 187, blue: 129)
    ]
    
    static func randomColorToCell() -> UIColor {
        return colorsCell[Int.random(in: 0...2)]
    }
}


extension UIColor {
    convenience init(red r: Int,green g: Int,blue b: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1)
    }
}
