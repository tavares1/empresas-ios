//
//  EnterpriseDetailView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseDetailView: UIView, ConfigurableView {
    
    let enterpriseStackView = EnterpriseStackView()
    
    let enterpriseCardView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let detailTextView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        textView.isEditable = false
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews([enterpriseCardView, detailTextView])
        enterpriseCardView.addSubview(enterpriseStackView)
        
    }
    
    func setupConstraint() {
    
        NSLayoutConstraint.activate([
            enterpriseCardView.topAnchor.constraint(equalTo: topAnchor),
            enterpriseCardView.leadingAnchor.constraint(equalTo: leadingAnchor),
            enterpriseCardView.trailingAnchor.constraint(equalTo: trailingAnchor),
            enterpriseCardView.heightAnchor.constraint(equalToConstant: 120)
        ])
        
        NSLayoutConstraint.activate([
            detailTextView.topAnchor.constraint(equalTo: enterpriseCardView.bottomAnchor, constant: 24),
            detailTextView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            detailTextView.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -16),
            detailTextView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            enterpriseStackView.centerYAnchor.constraint(equalTo: enterpriseCardView.centerYAnchor),
            enterpriseStackView.heightAnchor.constraint(equalToConstant: 60),
            enterpriseStackView.leadingAnchor.constraint(equalTo: enterpriseCardView.leadingAnchor),
            enterpriseStackView.trailingAnchor.constraint(equalTo: enterpriseCardView.trailingAnchor)
        ])
    }
    
    func setupInformationOf(Enteprise model: Enterprise) {
        enterpriseStackView.nameEnterpriseLabel.text = model.enterpriseName
        enterpriseCardView.backgroundColor = model.color
        detailTextView.text = model.description
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .white
    }
    
}
