//
//  EnterprisesDataSource.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseDataSource: NSObject, UICollectionViewDataSource {
    
    let enterprises: [Enterprise]
    
    var numberOfItems: Int {
        return self.enterprises.count
    }
    
    init(enterprises: [Enterprise]) {
        self.enterprises = enterprises
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EnterpriseCollectionViewCell.reuseIdentifier, for: indexPath) as? EnterpriseCollectionViewCell else { return UICollectionViewCell() }
        let enterprise = enterprises[indexPath.row]
        cell.setupCell(enterprise: enterprise)
        return cell
    }
    
    
}
