//
//  FeedView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

enum NavigationState {
    case large
    case normal
}

protocol FeedViewDelegate {
    func didAdaptNavigation(state: NavigationState)
    func didRecieveStringToSearch(value: String)
}

class FeedView: UIView, ConfigurableView {
    
    let searchCompanyTextField = SearchTextField()
    
    let enterpriseView = EnterprisesView()
    
    var delegate: FeedViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        translatesAutoresizingMaskIntoConstraints = false
        searchCompanyTextField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews([searchCompanyTextField, enterpriseView])
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            searchCompanyTextField.centerYAnchor.constraint(equalTo: topAnchor),
            searchCompanyTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            searchCompanyTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            searchCompanyTextField.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        NSLayoutConstraint.activate([
            enterpriseView.topAnchor.constraint(equalTo: searchCompanyTextField.bottomAnchor, constant: 16),
            enterpriseView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            enterpriseView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            enterpriseView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .white
    }
}

extension FeedView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.didAdaptNavigation(state: .normal)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = searchCompanyTextField.text as NSString? {
            let updatedText = text.replacingCharacters(in: range, with: string)
            let state: NavigationState = updatedText.isEmpty ? .large : .normal
            delegate?.didAdaptNavigation(state: state)
            delegate?.didRecieveStringToSearch(value: updatedText)
        }
        return true
    }
}
