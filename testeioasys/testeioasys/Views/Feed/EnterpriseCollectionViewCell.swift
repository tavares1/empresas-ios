//
//  EnterpriseCollectionViewCell.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import UIKit

class EnterpriseCollectionViewCell: UICollectionViewCell, ConfigurableView {
    
    static let reuseIdentifier = "EnterpriseTableViewCell"
    
    let stackView = EnterpriseStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.heightAnchor.constraint(equalToConstant: 60),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        clipsToBounds = true
        layer.cornerRadius = 8
    }
    
    func setupCell(enterprise: Enterprise) {
        stackView.nameEnterpriseLabel.text = enterprise.enterpriseName
        backgroundColor = enterprise.color
    }
}
