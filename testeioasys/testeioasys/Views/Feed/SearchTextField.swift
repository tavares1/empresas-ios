//
//  SearchTextField.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import UIKit

class SearchTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.placeholder = "Pesquise por Empresa"
        self.backgroundColor = .formTextFieldBackground
        self.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
        self.clipsToBounds = true
        self.layer.cornerRadius = 4
        self.setupIcon()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupIcon() {
        let iconImage = UIImage(named: "buscar")
        let iconImageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        iconImageView.image = iconImage
        let containerView = UIView(frame: CGRect(x: 20, y: 10, width: 40, height: 40))
        containerView.addSubview(iconImageView)
        self.leftViewMode = .always
        self.leftView = containerView
    }
}
