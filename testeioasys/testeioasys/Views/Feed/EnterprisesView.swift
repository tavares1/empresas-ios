//
//  EnterprisesView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

protocol EnteprisiesViewDelegate: class {
    func selectedItem(_ item: Int)
}

class EnterprisesView: UIView, ConfigurableView {
    
    let resultsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let noFoundLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Nenhum resultado encontrado"
        label.isHidden = true
        label.textAlignment = .center
        return label
    }()
    
    lazy var enterprisesCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 4
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(EnterpriseCollectionViewCell.self, forCellWithReuseIdentifier: EnterpriseCollectionViewCell.reuseIdentifier)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        return collectionView
    }()
    
    var delegate: EnteprisiesViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupDataSource(dataSource: EnterpriseDataSource) {
        enterprisesCollectionView.dataSource = dataSource
        enterprisesCollectionView.reloadData()
        resultsLabel.text = dataSource.numberOfItems > 0 ? "\(dataSource.numberOfItems) resultados encontrados" : ""
    }
    
    func buildViewHierarchy() {
        addSubviews([resultsLabel, enterprisesCollectionView, noFoundLabel])
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            resultsLabel.topAnchor.constraint(equalTo: topAnchor),
            resultsLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            resultsLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            enterprisesCollectionView.topAnchor.constraint(equalTo: resultsLabel.bottomAnchor, constant: 16),
            enterprisesCollectionView.leadingAnchor.constraint(equalTo: resultsLabel.leadingAnchor),
            enterprisesCollectionView.widthAnchor.constraint(equalTo: resultsLabel.widthAnchor),
            enterprisesCollectionView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            noFoundLabel.topAnchor.constraint(equalTo: enterprisesCollectionView.topAnchor, constant: 160),
            noFoundLabel.leadingAnchor.constraint(equalTo: enterprisesCollectionView.leadingAnchor),
            noFoundLabel.trailingAnchor.constraint(equalTo: enterprisesCollectionView.trailingAnchor),
            noFoundLabel.centerXAnchor.constraint(equalTo: enterprisesCollectionView.centerXAnchor),
            noFoundLabel.heightAnchor.constraint(equalToConstant: 24),
        ])
    }
    
    func setupAdditionalConfiguration() {
        enterprisesCollectionView.backgroundColor = .white
    }
    
    func verifyIfShoudDisplayNoFoundLabel(isSearching: Bool, dataSourceIsEmpty: Bool) {
        if isSearching && dataSourceIsEmpty {
            noFoundLabel.isHidden = false
        } else {
            noFoundLabel.isHidden = true
        }
    }
}

extension EnterprisesView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.size.width, height: 120)
    }
}

extension EnterprisesView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectedItem(indexPath.row)
    }
}
