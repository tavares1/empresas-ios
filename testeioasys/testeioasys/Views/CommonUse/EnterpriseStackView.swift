//
//  EnterpriseStackView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 28/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseStackView: UIStackView, ConfigurableView {

    let iconEntepriseImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "enterprise_icon"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        return imageView
    }()
    
    let nameEnterpriseLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "EMPRESA XY"
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    let backgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
        addArrangedSubview(iconEntepriseImageView)
        addArrangedSubview(nameEnterpriseLabel)
        axis = .vertical
        distribution = .fillEqually
        alignment = .center
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(backgroundView)
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        
    }

}
