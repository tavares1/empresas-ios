//
//  BottomCurvedView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class BottomCurvedView: UIView {
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        let rect = self.bounds
        let y = rect.size.height - 80
        let curveTo = rect.size.height
        
        let maskForPath = CAShapeLayer()
        maskForPath.path = getPath(y: y, rect: rect, curveTo: curveTo)
        self.layer.mask = maskForPath
        
    }
    
    fileprivate func getPath(y: CGFloat, rect: CGRect, curveTo: CGFloat) -> CGPath {
        let bezier = UIBezierPath()
        bezier.move(to: CGPoint(x: 0.0, y: y))
        bezier.addQuadCurve(to: CGPoint(x: rect.size.width, y: y), controlPoint: CGPoint(x: rect.size.width/2.0, y: curveTo))
        bezier.addLine(to: CGPoint(x: rect.size.width, y: 0.0))
        bezier.addLine(to: CGPoint(x: 0.0, y: 0.0))
        bezier.close()
        return bezier.cgPath
    }
}
