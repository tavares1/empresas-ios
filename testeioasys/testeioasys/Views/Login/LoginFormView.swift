//
//  LoginFormView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

enum TextFieldFormState {
    case interacting
    case normal
}

protocol InteractableFormViewDelegate {
    func interactionWithTextFieldForm(state: TextFieldFormState)
}

protocol LoginFormViewDelegate {
    func shouldLogin(email: String, password: String)
}

class LoginFormView: UIView, ConfigurableView {
    
    var isShowing: Bool = false
    
    lazy var passwordPropertieSecurityButton: UIButton = {
        let button = UIButton(type: .custom)
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        
        button.frame = CGRect(x: CGFloat(self.passwordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        
        var image = UIImage(named: PasswordTextFieldState.hidePassword.rawValue)
        
        button.setImage(image, for: .normal)
        
        button.addTarget(self, action: #selector(didChangePasswordTextFieldSecurityPropertie), for: .touchUpInside)
        
        return button
    }()
    
    let emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Email"
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .label
        return label
    }()
    
    let emailTextField = FormViewTextField()
    
    let passwordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.text = "Senha"
        label.textColor = .label
        return label
    }()
    
    let passwordTextField = FormViewTextField()
    
    lazy var formStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [emailLabel,emailTextField,passwordLabel,passwordTextField])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    let wrongCrendetialsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Credenciais incorretas"
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = .red
        label.isHidden = true
        label.textAlignment = .right
        return label
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ENTRAR", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = .loginButtonColor
        button.clipsToBounds = true
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(didTapLoginButton(_:)), for: .touchUpInside)
        return button
    }()
    
    var interactableDelegate: InteractableFormViewDelegate?
    var delegate: LoginFormViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        setupView()
        setupPasswordTextfield()
        setupDelegateTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews([formStackView, wrongCrendetialsLabel ,loginButton])
    }
    
    func setupConstraint() {
        NSLayoutConstraint.activate([
            formStackView.topAnchor.constraint(equalTo: topAnchor),
            formStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            formStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            wrongCrendetialsLabel.topAnchor.constraint(equalTo: formStackView.bottomAnchor, constant: 4),
            wrongCrendetialsLabel.heightAnchor.constraint(equalToConstant: 16),
            wrongCrendetialsLabel.leadingAnchor.constraint(equalTo: formStackView.leadingAnchor),
            wrongCrendetialsLabel.trailingAnchor.constraint(equalTo: formStackView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: formStackView.bottomAnchor, constant: 40),
            loginButton.widthAnchor.constraint(equalToConstant: 310),
            loginButton.heightAnchor.constraint(equalToConstant: 50),
            loginButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            loginButton.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupAdditionalConfiguration() {
        
    }
    
    @objc func didTapLoginButton(_ sender: UIButton) {
        endEditing(true)
        if let email = emailTextField.text, let password = passwordTextField.text {
            delegate?.shouldLogin(email: email, password: password)
        } else {
            shouldDisplayError()
        }
    }
    
    func shouldDisplayError() {
        emailTextField.errorState()
        passwordTextField.errorState()
        wrongCrendetialsLabel.isHidden = false
    }
    
    func setupDelegateTextField() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
}

enum PasswordTextFieldState: String {
    case showPassword
    case hidePassword
}

extension LoginFormView {
    
    func setupFromState(_ state: PasswordTextFieldState) {
        switch state {
        case .showPassword:
            passwordTextField.isSecureTextEntry = false
        case .hidePassword:
            passwordTextField.isSecureTextEntry = true
        }

        self.passwordPropertieSecurityButton.setImage(UIImage(named: state.rawValue), for: .normal)
        
    }

    func setupPasswordTextfield() {
        passwordTextField.isSecureTextEntry = true
        passwordTextField.rightViewMode = UITextField.ViewMode.always
        passwordTextField.rightView = passwordPropertieSecurityButton
        passwordTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(didCallTextfieldShouldReturn))
    }
    
    @objc func didCallTextfieldShouldReturn() {
        interactableDelegate?.interactionWithTextFieldForm(state: .normal)
    }
    
    @objc func didChangePasswordTextFieldSecurityPropertie() {
        isShowing = !isShowing
        isShowing ? setupFromState(.showPassword) : setupFromState(.hidePassword)
    }
}

extension LoginFormView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        interactableDelegate?.interactionWithTextFieldForm(state: .interacting)
        return true
    }
}
