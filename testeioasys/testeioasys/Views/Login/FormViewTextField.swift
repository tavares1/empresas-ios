//
//  FormViewTextField.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

class FormViewTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 8
        self.clipsToBounds = true
        self.font = UIFont.systemFont(ofSize: 16, weight: .light)
        self.backgroundColor = .formTextFieldBackground
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 20))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.autocapitalizationType = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func errorState() {
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 1.0
        self.rightViewMode = .always
    
        let button = UIButton(frame: CGRect(x: 0, y: 5, width: 20, height: 20))
        button.setImage(UIImage(named: "customClearButton"), for: .normal)
        button.addTarget(self, action: #selector(clear(_:)), for: .touchUpInside)
        let imageViewContainer = UIView(frame: CGRect(x: 10, y: 0, width: 30, height: 30))
        imageViewContainer.addSubview(button)
        self.rightView = imageViewContainer
    }
    
    @objc func clear(_ sender: AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
}
