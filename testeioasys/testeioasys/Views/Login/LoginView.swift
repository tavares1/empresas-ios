//
//  LoginView.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import UIKit

class LoginView: UIView, ConfigurableView {

    let loginHeaderView = LoginHeaderView()
    let loginFormView = LoginFormView()
    
    var heightConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        loginFormView.interactableDelegate = self
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews([loginHeaderView, loginFormView])
    }
    
    func setupConstraint() {
        heightConstraint = loginHeaderView.heightAnchor.constraint(equalToConstant: 240)
        NSLayoutConstraint.activate([
            loginHeaderView.topAnchor.constraint(equalTo: topAnchor),
            loginHeaderView.trailingAnchor.constraint(equalTo: trailingAnchor),
            loginHeaderView.leadingAnchor.constraint(equalTo: leadingAnchor),
            self.heightConstraint
        ])
        
        NSLayoutConstraint.activate([
            loginFormView.topAnchor.constraint(equalTo: loginHeaderView.bottomAnchor, constant: -16),
            loginFormView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            loginFormView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            loginFormView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.4)
        ])
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .white
    }
}

extension LoginView: InteractableFormViewDelegate {
    func interactionWithTextFieldForm(state: TextFieldFormState) {
        switch state {
        case .interacting:
            setNeedsUpdateConstraints()
            UIView.animate(withDuration: 0.5) {
                self.heightConstraint.constant = 160
                self.layoutIfNeeded()
            }
        case .normal:
            UIView.animate(withDuration: 0.5) {
                self.heightConstraint.constant = 240
                self.layoutIfNeeded()
            }
        }
    }
}
