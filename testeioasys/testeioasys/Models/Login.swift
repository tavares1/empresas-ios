//
//  Login.swift
//  testeioasys
//
//  Created by Lucas Tavares on 26/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    let investor: Investor
    let success: Bool
}
