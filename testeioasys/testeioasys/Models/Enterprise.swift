//
//  Enterprise.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation
import UIKit

struct EnterpriseResponse: Codable {
    let enterprises: [Enterprise]
}

struct Enterprise: Codable {
    let id: Int
    let enterpriseName: String
    let description: String
    let city: String
    let country: String
    let sharePrice: Int
    let photo: String?
    let color: UIColor = UIColor.randomColorToCell()
    
    enum CodingKeys: String, CodingKey {
        case enterpriseName = "enterprise_name"
        case sharePrice = "share_price"
        case id
        case city
        case country
        case description
        case photo
    }
}
