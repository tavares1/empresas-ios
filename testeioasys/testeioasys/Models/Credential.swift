//
//  Credential.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

struct Credential: Codable {
    let email: String
    let password: String
}
