//
//  Investor.swift
//  testeioasys
//
//  Created by Lucas Tavares on 27/03/20.
//  Copyright © 2020 Lucas Tavares. All rights reserved.
//

import Foundation

struct Investor: Codable {
    let id: Int
    let investor_name: String
    let email: String
    let city: String
    let country: String
    let balance: Int
    let photo: String
    let portfolio_value: Int
    let first_access: Bool
    let super_angel: Bool
}
